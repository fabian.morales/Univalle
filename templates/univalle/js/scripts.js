(function(window, $){         
    function scrollTo(target) {
        //var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $(window).resize(function() {                
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul:first-child").hide();
            }
            else{                    
                $("#header-top ul:first-child").show();
            }
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul:first-child").toggle("slow");
        });
        
        $("#lnk_top").click(function(e){
            e.preventDefault();
            scrollTo("#top-link");
        });
        
        $("#banner").lightSlider({
            item: 1,
            pager: true,
            enableDrag: true,
            controls: true,
            loop: true
        });
        
        $(document).foundation();
    });
})(window, jQuery);