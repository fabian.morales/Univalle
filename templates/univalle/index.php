<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation-flex.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/fonts/foundation-icons.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/lightSlider.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/featherlight/featherlight.css');

$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/foundation.min.js');

$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/jquery.lightSlider.min.js');
$doc->addScript('templates/'.$this->template.'/js/featherlight/featherlight.js');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');

// Add current user information
$user = JFactory::getUser();
$menu = $app->getMenu();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
            <?php if($this->countModules('in-banner')) : ?>
                <jdoc:include type="modules" name="in-banner" style="xhtml" />
            <?php endif; ?>
            
                <?php
        if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="component">
            <div class="row">
                <div class="small-12 columns">
                    <jdoc:include type="component" />
                </div>
            </div>
        </div>        
    <?php endif; ?>
    
    
    
    <div id="top-link"></div>
    <header>
        <div class="row expanded">
            <div class="medium-2 columns"><img src="templates/univalle/img/logo_escuela.png" class="logo_escuela" /></div>
            <div class="medium-6 columns"><img src="templates/univalle/img/logo_univalle.png" class="img_fullwidth" /></div>
            <div class="medium-4 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <jdoc:include type="modules" name="menu-header" style="xhtml" />
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <section>
        <div class="row expanded collapse">
            <div class="medium-2 columns separador_rojo">&nbsp;</div>
            <div class="medium-10 columns">
                <?php if($this->countModules('main-menu')) : ?>
                    <nav id="top-menu">
                        <jdoc:include type="modules" name="main-menu" style="xhtml" />
                    </nav>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <section>
        <div class="row expanded collapse">
            <div class="medium-2 columns sidebar">
                <jdoc:include type="modules" name="sidebar" style="xhtml" />
                
                <ul class="items">
                    <li><a><img src="templates/univalle/img/galeria.png" class="img_fullwidth" /><br />Galería Multimedia</a></li>
                    <li><a><img src="templates/univalle/img/calendario.png" class="img_fullwidth" /><br />Calendario</a></li>
                </ul>
            </div>
            <div class="medium-10 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <jdoc:include type="modules" name="banner" style="xhtml" />
                    </div>
                    <div class="small-12 columns">
                        <jdoc:include type="modules" name="sub-banner" style="xhtml" />
                    </div>
                </div>
                
                <?php if ($menu->getActive() != $menu->getDefault()) : ?>
                    <div class="component">
                        <div class="row">
                            <div class="small-12 columns">
                                <jdoc:include type="component" />
                            </div>
                        </div>
                    </div>        
                <?php endif; ?>
                
                <div class="row">
                    <div class="small-12 columns">
                        <jdoc:include type="modules" name="sub-content" style="xhtml" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="row expanded info">
            <div class="medium-2 columns"><img class="logo" src="templates/univalle/img/logo_footer.png" /></div>
            <div class="medium-2 column lateral align-self-middle">
                <jdoc:include type="modules" name="footer-1" style="xhtml" />
            </div>
            <div class="medium-6 column lateral align-self-middle">
                <jdoc:include type="modules" name="footer-2" style="xhtml" />
            </div>
            <div class="medium-2 column align-self-middle">
                <ul class="redes">
                    <li><a href="#"><i class="fi-social-facebook"></i></a></li>
                    <li><a href="#"><i class="fi-social-twitter"></i></a></li>
                    <li><a href="#"><i class="fi-social-instagram"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="small-12 text-center">
                Universidad del Valle. Cali, Colombia &copy;1994-2016
            </div>
        </div>
    </footer>
</body>
</html>
